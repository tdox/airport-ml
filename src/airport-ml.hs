{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE OverloadedStrings #-}

module Main where

import Lib

-- base
import Control.Exception.Base (assert)
import Data.List (nub, sort)
import Data.Maybe (fromMaybe)

-- bytestring
-- import qualified Data.ByteString.Lazy as BL

-- random
import System.Random (randomRs, newStdGen, StdGen)

-- vector
import Data.Vector (Vector, (!))
import qualified Data.Vector as V

-- text
import Data.Text (Text, unpack)
import qualified Data.Text as T
import qualified Data.Text.IO as TIO

-- time
import Data.Time (UTCTime, defaultTimeLocale, parseTimeM)

--------------------------------------------------------------------------------

main :: IO ()
main = io3

io_main :: IO ()
io_main = do
  putStrLn "start"
  
  legs <- readFromFile "/home/tad/tmp/leg_data/legs.txt" :: IO [Leg]
  let legs10 = take 10 legs
  mapM_ (putStrLn . show) legs10

  let nLegs = length legs
  putStrLn $ "nLegs: " ++ show nLegs

  airports <- readFromFile "/home/tad/tmp/leg_data/airports.txt" :: IO [Airport]
  let aps10 = take 10 airports
  mapM_ (putStrLn . show) aps10

  let nAirports = length airports
  putStrLn $ "nAirports: " ++ show nAirports

  g0 <- newStdGen
 
  let nInputs = 10000
  let rawInputs = mkRawInputs nInputs g0 airports
  putStrLn $ "rawInputs: "
--  mapM_ (putStrLn . show) rawInputs

  let dists = map computeDistanceMi rawInputs
  putStrLn $ "outputs: "
--  mapM_ (putStrLn . show) dists

  let input1s = map mkInput1 rawInputs
  putStrLn $ "input1s: "
--  mapM_ (putStrLn . show) input1s

  let nInputs = length input1s
  putStrLn $ "nInputs: " ++ show nInputs
  
  putStrLn "done"
