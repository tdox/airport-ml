{-# OPTIONS_GHC -Wall #-}

{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE OverloadedStrings #-}

module Lib where

-- base
import Control.Exception.Base (assert)
import Data.Maybe (fromJust, fromMaybe, isJust)
import Control.Monad (forM_)

-- containers
import Data.Map (Map)
import qualified Data.Map as Map

-- hmatrix
import Numeric.LinearAlgebra (Matrix, R, (¿), (><), (<.>), (<>), (<\>), (#>), fromColumns, fromLists
                             , fromRows
                             , norm_2, range
                             , size, toColumns, toList, toRows, tr)


-- random
import System.Random (randomRs, StdGen, mkStdGen, newStdGen, split)

-- statistics
import Statistics.Sample (mean, stdDev)

-- vector
import Data.Vector (Vector, (!))
import qualified Data.Vector as V
import qualified Data.Vector.Storable as SV

-- text
import Data.Text (Text, unpack)
import qualified Data.Text as T
import qualified Data.Text.IO as TIO

-- time
import Data.Time (UTCTime, defaultTimeLocale, parseTimeM)

--------------------------------------------------------------------------------

class TextRead a where
  readText :: Text -> a

class HasId a where
  getId :: a -> Text
  
--------------------------------------------------------------------------------

data Leg = Leg
  { lId :: !Text
  , lTenantId :: !Text
  , lAircraftId :: !Text
  , lOriginAirportId :: !Text
  , lDestinationAirportId :: !Text
  , lDepartureInst :: !UTCTime
  , lArrivalInst :: !UTCTime
  } deriving (Eq, Show)

instance HasId Leg where
  getId = lId


type CountryCode = Text
type UsStateCode = (Char, Char)

data ICAO = ICAO Text deriving (Eq, Ord)
data IATA = IATA Text deriving (Eq, Ord)
data FAA  = FAA  Text deriving (Eq, Ord)

instance Show ICAO where
  show (ICAO t) = "ICAO:" ++ unpack t

instance Show IATA where
  show (IATA t) = "IATA:" ++ unpack t

instance Show FAA where
  show (FAA t) = "FAA:" ++ unpack t

data CountryAirportCode = CAC CountryCode Text
                           deriving (Eq, Ord, Show)


data AirportCode = ICAOac ICAO
                 | IATAac IATA
                 | FAAac  FAA
                 | CACac CountryAirportCode
                 deriving (Eq, Ord, Show)



data LatLonDeg = LatLonDeg
  { _latDeg :: !Double
  , _lonDeg :: !Double
  } deriving (Eq, Ord, Show)


data AirportCodes = AirportCodes
  { acFAA  :: Maybe FAA
  , acICAO :: Maybe ICAO
  , acIATA :: Maybe IATA
  , acCAC  :: Maybe CountryAirportCode
  } deriving (Eq, Ord, Show)



data Airport = Airport
  { apId :: !Text
  , apCodes :: !AirportCodes
  , apLatLon :: !LatLonDeg
  } deriving (Eq, Show)


instance HasId Airport where
  getId = apId


data RawInput = RawInput -- degrees
  { riOriginLat :: !Double
  , riOrignLon :: !Double
  , riDestLat :: !Double
  , riDestLon :: !Double
  } deriving (Show)

data Input1 = Input1
  { iOriginLat :: !Double -- [-pi/2, +pi/2]
  , iOriginLon :: !Double -- [-pi,   +pi]
  , iDeltaLat :: !Double  -- [-pi/2, +pi/2] 
  , iDeltaLon :: !Double  -- [-pi,   +pi] (shortest path)
  } deriving Show




data NormalizedTrainingData = NormalizedTrainingData
  { _meansStdDevsX :: [(Double, Double)]
  , _X :: Matrix R
  , ntdMeanStdDevY :: (Double, Double)
  , _y :: SV.Vector R
  } deriving Show

{-
data TrainingDataSummary = TrainingDataSummary
  { _meansStdDevsX :: [(Double, Double)]
  , _X :: Matrix R
  , _meanStdDevY :: (Double, Double)
  , _y :: SV.Vector R
  } deriving Show
-}



--------------------------------------------------------------------------------

relTol, absTol :: Double
relTol = 1.0e-9
absTol = 1.0e-9




readLegs :: FilePath -> IO [Leg]
readLegs fp = do
  lns <- T.lines <$> TIO.readFile fp :: IO [Text]
  pure $ map readLeg $ tail lns
  

readLinesFromFile :: (Text -> a) -> FilePath -> IO [a]
readLinesFromFile reader fp = do
  lns <- T.lines <$> TIO.readFile fp :: IO [Text]
  pure $ map reader $ tail lns

readFromFile :: (TextRead a) => FilePath -> IO [a]
readFromFile fp = do
  lns <- T.lines <$> TIO.readFile fp :: IO [Text]
  pure $ map readText $ tail lns
  

readLeg :: Text -> Leg
readLeg ln =
  Leg id_ lTenantId' lAircraftId'
      lOriginAirportId' lDestinationAirportId'
      (readUTCTime (unpack lDepartureInstTxt))
      (readUTCTime (unpack lArrivalInstTxt))
      
  where
    [  id_
     , lTenantId'
     , lAircraftId'
     , lOriginAirportId'
     , lDestinationAirportId'
     , lDepartureInstTxt
     , lArrivalInstTxt
     ] = T.splitOn "\t" ln


instance TextRead Leg where
  readText = readLeg


readUTCTime :: String-> UTCTime
readUTCTime s = utcTime
  where
    mUtcTime =  parseTimeM False defaultTimeLocale "%F %T" $ s :: Maybe UTCTime
    utcTime = fromMaybe (error ("readUTCTime: could not parse: " ++ s)) mUtcTime


mkAirportCodes :: Text -> Text -> Text -> AirportCodes
mkAirportCodes icaoTxt iataTxt faaTxt =
  AirportCodes mFaa mIcao mIata Nothing
  where
    mFaa  = FAA  <$> checkNull faaTxt  :: Maybe FAA
    mIcao = ICAO <$> checkNull icaoTxt :: Maybe ICAO
    mIata = IATA <$> checkNull iataTxt :: Maybe IATA


checkNull :: Text -> Maybe Text
checkNull txt =
  case txt of
    "NULL" -> Nothing
    t -> Just t
  

readAirport :: Text -> Airport
readAirport ln = Airport apId_ codes latLon
  where
    [  apId_
     , icaoTxt
     , iataTxt
     , faaTxt
     , _ -- elevation
     , latTxt
     , lonTxt
     ] = T.splitOn "\t" ln
     
    codes = mkAirportCodes icaoTxt iataTxt faaTxt
    latLon = LatLonDeg (read (unpack latTxt)) (read (unpack lonTxt))


mkLatLonDeg :: Double -> Double -> LatLonDeg
mkLatLonDeg lat lon =
  LatLonDeg
    (assert (lat >= -90 && lat <= 90) lat)
    (assert (lon >= -180 && lon <= 180) lon)
    
instance TextRead Airport where
  readText = readAirport


degreesToRadians :: Double -> Double
degreesToRadians deg = deg * pi / 180.0

distanceMiles :: LatLonDeg -> LatLonDeg -> Double
distanceMiles (LatLonDeg p1Deg l1Deg)
              (LatLonDeg p2Deg l2Deg) = d
  where
    p1 = degreesToRadians p1Deg
    l1 = degreesToRadians l1Deg
    
    p2 = degreesToRadians p2Deg
    l2 = degreesToRadians l2Deg

    halfDeltaP = (p2 - p1) / 2.0
    halfDeltaL = (l2 - l1) / 2.0

    sinHalfDeltaP = sin(halfDeltaP)
    sinHalfDeltaL = sin(halfDeltaL)

    a = sinHalfDeltaP * sinHalfDeltaP
      + cos p1 * cos p2 * sinHalfDeltaL * sinHalfDeltaL

    c = 2.0 * atan2 (sqrt a) (sqrt (1.0 - a))

    earthRad = 3959 -- miles

    d = c * earthRad
    

distanceBetweenAirports :: Airport -> Airport -> Double
distanceBetweenAirports ap1 ap2 = distanceMiles latLon1 latLon2
  where
    [latLon1, latLon2] = map apLatLon [ap1, ap2]


mkPairs :: [a] -> [(a, a)]
mkPairs [] = []
mkPairs [_] = []
mkPairs (x:y:xs) = (x, y) : mkPairs xs

mkRawInput :: Vector Airport -> (Int, Int) -> RawInput
mkRawInput airports (i, j) = assert (rawInputIsValid ri) ri
  where
    latLoni = apLatLon $ airports ! i
    lati = _latDeg latLoni
    loni = _lonDeg latLoni

    latLonj = apLatLon $ airports ! j
    latj = _latDeg latLonj
    lonj = _lonDeg latLonj

    ri = RawInput lati loni latj lonj


mkRawInputs :: Int -> StdGen -> [Airport] -> [RawInput]
mkRawInputs n g airports =  take n $ map (mkRawInput aps) ijs
  where
    aps = V.fromList airports
    ijs = mkPairs $ randomRs (0, V.length aps - 1) g


computeDistanceMi :: RawInput -> Double
computeDistanceMi (RawInput lat1 lon1 lat2 lon2) = distanceMiles latLon1 latLon2
  where
    latLon1 = LatLonDeg lat1 lon1
    latLon2 = LatLonDeg lat2 lon2

mkInput1 :: RawInput -> Input1
mkInput1 (RawInput lat1deg lon1deg lat2deg lon2deg) =
  Input1 lat1 lon1 deltaLat deltaLon
  where
    lat1 = degreesToRadians lat1deg
    lon1 = degreesToRadians lon1deg

    lat2 = degreesToRadians lat2deg
    lon2 = degreesToRadians lon2deg

-- **    deltaLat = lat2 - lat1
-- **     deltaLon = computeDeltaLon lon1 lon2
    deltaLat = abs $ lat2 - lat1
    deltaLon = abs $ computeDeltaLon lon1 lon2


latDegreesAreValid :: Double -> Bool
latDegreesAreValid deg = deg <= 90 && deg >= -90

lonDegreesAreValid :: Double -> Bool
lonDegreesAreValid deg = deg <= 180 && deg >= -180

rawInputIsValid :: RawInput -> Bool
rawInputIsValid (RawInput lat1 lon1 lat2 lon2) =
      latDegreesAreValid lat1
   && lonDegreesAreValid lon1
   && latDegreesAreValid lat2
   && lonDegreesAreValid lon2



computeDeltaLon :: Double -> Double -> Double -- radians
computeDeltaLon lonPosNeg1 lonPosNeg2 =
  assert (deltaLon >= (-pi) && deltaLon <= pi) deltaLon
  where
    twoPi = 2 * pi
    lonPos1 = rescale (-pi) pi 0 twoPi lonPosNeg1
    lonPos2 = rescale (-pi) pi 0 twoPi lonPosNeg2

    deltaLon1 = lonPos2 - lonPos1

    deltaLon =
      if deltaLon1 >= 0
      then
        if deltaLon1 <= pi
        then deltaLon1
        else twoPi - deltaLon1 -- (lon1 + (2pi - lon2))
      else
        if (- deltaLon1) <= pi
        then deltaLon1
        else deltaLon1 + twoPi -- (lon2 + (twoPi - lon1))


randomLatLonDegs :: StdGen -> [LatLonDeg]
randomLatLonDegs g = zipWith mkLatLonDeg lats lons
  where
    (g1, g2) = split g
    lats = randomRs (-90, 90) g1
-- **     lons = randomRs (-180, 180) g2
    lons = copy $ randomRs (-180, 180) g2

copy :: [a] -> [a]
copy [] = []
copy (x0:xs) = x0 : x0 : copy xs

rescale :: Double -> Double -> Double -> Double -> Double -> Double
rescale x0 x1 y0 y1 x = y
  where
    collapsedX = nearlyEqual x0 x1
    collapsedY = nearlyEqual y0 y1

    y = if collapsedX
        then x
        else if collapsedY
             then y0
             else y0 + (x - x0) * (y1 - y0) / (x1 - x0)

nearlyEqual :: Double -> Double -> Bool
nearlyEqual x y = abs x + abs y < absTol || relativeNearlyEqual x y

relativeNearlyEqualWithNorm :: Double -> Double -> Double -> Bool
relativeNearlyEqualWithNorm x y norm = abs (x - y) < abs (norm * relTol)


relativeNearlyEqual :: Double -> Double -> Bool
relativeNearlyEqual x y = relativeNearlyEqualWithNorm x y norm
  where
    norm = sqrt $ x*x + y*y


generateTrainingData :: StdGen -> Int -> Int
                     -> ([LatLonDeg], NormalizedTrainingData)
                     
generateTrainingData g nData nPow = (latLonDegs, normalizedTrainingData)
  where
    latLonDegs = take (2*nData) $ randomLatLonDegs g :: [LatLonDeg]
    
    normalizedTrainingData = mkTrainingData latLonDegs nPow
      :: NormalizedTrainingData
        

mkTrainingData :: [LatLonDeg] -> Int -> NormalizedTrainingData
                 
mkTrainingData latLonDegs nPow =
-- **  NormalizedTrainingData meansStdDevsX _X1 (mn, sd) y
  NormalizedTrainingData meansStdDevsX _X0 (mn, sd) y
  where
    latLonPairs = mkPairs latLonDegs :: [(LatLonDeg, LatLonDeg)]

    {-
    rows0  :: [RawInput] =
      map (\(LatLonDeg lat1 lon1, LatLonDeg lat2 lon2) ->
              RawInput lat1 lon1 lat2 lon2)
           latLonPairs
           

    rows2 :: [Input1]
      = map mkInput1 rows0
    
    rows3 :: [Vector R] =
      map (\(Input1 x1 x2 x3 x4) -> V.fromList [x1, x2, x3, x4]) rows2

             

    rowsWithPowers = map (\row -> mkNewVars row nPow) rows3 :: [[Double]]
-}

    rowsWithPowers = map (\(latLon1, latLon2) -> mkTrainingRow nPow latLon1 latLon2) latLonPairs

    _X0 = fromLists rowsWithPowers

    (meansStdDevsX, _X1) = normalizeByColumns _X0
      :: ([(Double, Double)], Matrix R)

    distsMi :: SV.Vector R
      = SV.fromList $ map (\(p1, p2) -> distanceMiles p1 p2) latLonPairs

    (mn, sd, y) :: (Double, Double, SV.Vector R)
      = normalizeVector distsMi



                 

solveNormalEquation :: Matrix R -> SV.Vector R -> SV.Vector R
solveNormalEquation _X y = (_Xtr <> _X) <\> (_Xtr #> y)
  where
    _Xtr = tr _X


predictDistance :: LatLonDeg -> LatLonDeg
                -> SV.Vector R -> (Double, Double) -> Int
                -> Double
                
predictDistance latLon1 latLon2 w meanStdDevY nPow = dist
  where
    mn = fst meanStdDevY
    sd = snd meanStdDevY

{-
    rawInput =
      RawInput (_latDeg latLon1) (_lonDeg latLon1)
               (_latDeg latLon2) (_lonDeg latLon2)
      
    Input1 x1 x2 x3 x4  = mkInput1 rawInput :: Input1
    inputV = V.fromList [x1, x2, x3, x4]
    vars = mkNewVars inputV nPow :: [Double]
-}
    vars = mkTrainingRow nPow latLon1 latLon2
    varsV = SV.fromList vars :: SV.Vector R
    distNorm = w <.> varsV
    dist = distNorm -- ** rescale (-1) 1 (mn - sd) (mn + sd) distNorm


mkTrainingRow :: Int -> LatLonDeg -> LatLonDeg -> [Double]
mkTrainingRow nPow latLon1 latLon2 = vars
  where
    rawInput =
      RawInput (_latDeg latLon1) (_lonDeg latLon1)
               (_latDeg latLon2) (_lonDeg latLon2)

    Input1 x1 x2 x3 x4  = mkInput1 rawInput :: Input1
    inputV = V.fromList [x1, x2, x3, x4]
    vars = mkNewVars inputV nPow :: [Double]


computeExactAndPredictedDistance :: LatLonDeg -> LatLonDeg
                                 -> SV.Vector R -> (Double, Double) -> Int
                                 -> (Double, Double)

computeExactAndPredictedDistance latLon1 latLon2 w meanStdDevY nPow =
  (dist, distP)
  where
    distP = predictDistance latLon1 latLon2 w meanStdDevY nPow
    dist = distanceMiles latLon1 latLon2


mkIcaoAirportMap :: [Airport] -> Map ICAO Airport
mkIcaoAirportMap aps =
  Map.fromList $ map (\ap -> (fromJust (acICAO (apCodes ap)), ap)) aps1
  where
    aps1 = filter hasIcao aps

hasIcao :: Airport -> Bool
hasIcao ap = isJust $ acICAO $ apCodes ap


io2 :: IO ()
io2 = do
  putStrLn "io2"
--  airports <- readFromFile "/Users/tad/tmp/leg_data/airports.txt"
  airports <- readFromFile "/home/tad/tmp/leg_data/airports.txt"
    :: IO [Airport]

  let nAirports = length airports

  putStrLn $ "nAirports: " ++ show nAirports

  let apMap = mkIcaoAirportMap airports

  let lax = apMap Map.! ICAO "KLAX"

  putStrLn $ "lax: " ++ show lax
  

  putStrLn "done"

--------------------------------------------------------------------------------

-- from values of existing variables, (x, y, z, ..) compute values of new
-- variables (x, y, z, x^2, xy, xz, y^2, yz, z^2) for a given
-- power, nPow (2, in this example)

-- see test1.hs for examples

mkNewVars :: Vector Double -> Int -> [Double]
mkNewVars xs0 nPow = xs1
  where
    nVars = V.length xs0
    powss = mkPows nVars nPow :: [[Int]]
    xs1 = map (\pows -> evalForIdxs xs0 pows) powss :: [Double]


prependOneToLists :: Int -> Int -> [[Int]] -> [[Int]]
prependOneToLists n x yss =
  filter (\l -> sum l <= n) $ map (x:) yss


prependToLists :: Int -> [Int] -> [[Int]] -> [[Int]]
prependToLists n xs yss = concatMap (\x -> prependOneToLists n x yss) xs


mkFIter :: Int -> ([[Int]] -> [[Int]])
mkFIter nPow = prependToLists nPow [0 .. nPow]


mkPows :: Int -> Int -> [[Int]]
mkPows nVar nPow = (iterate fIter yss0) !! (nVar - 1)
  where
    fIter = mkFIter nPow :: [[Int]] -> [[Int]]
    pows = [0 .. nPow]   :: [Int]              -- [1, 2, ..., nPow]
    yss0 = pure <$> pows :: [[Int]]            -- [[1], [2], ... [nPow]]


evalPowAtIndx :: Vector Double -> (Int, Int) -> Double
evalPowAtIndx xs (idx, iPow) = (xs ! idx) ^ iPow


evalForIdxs :: Vector Double -> [Int] -> Double
evalForIdxs xs powAtIdxs = product vals
  where
    idxPows = zip [0 ..] powAtIdxs :: [(Int, Int)] -- [(idx, iPow)]
    vals = map (\idxPow -> evalPowAtIndx xs idxPow) idxPows :: [Double]

--------------------------------------------------------------------------------

    
columnsMeanAndStdDev :: Matrix R -> [(Double, Double)]
columnsMeanAndStdDev m = zip means stdDevs
  where
    cols = toColumns m :: [SV.Vector R]
    means = map mean $ cols
    stdDevs = map stdDev $ cols

normalizeVector :: SV.Vector R -> (Double, Double, SV.Vector R)
-- normalizeVector v0 = (mn, sd, v1) **
normalizeVector v0 = (mn, sd, v0)
  where
    mn = mean v0
    sd = stdDev v0
    v1 = SV.map (\x -> rescale (mn - sd) (mn + sd) (-1) 1 x) v0
    

normalizeByColumns :: Matrix R -> ([(Double, Double)], Matrix R)
normalizeByColumns m = (zip means stdDevs, fromColumns cols1)
  where
    cols0 = toColumns m :: [SV.Vector R]
    means = map mean $ cols0
    stdDevs = map stdDev $ cols0
    msdcs = zip3 means stdDevs cols0 :: [(Double, Double, SV.Vector R)]
    
    cols1 =
      map (\(mn, sd, col) ->
              (SV.map (\el -> rescale (mn - sd) (mn + sd) (-1) 1 el
                      ) col)
          ) msdcs
    
unnormalize' :: ([(Double, Double)], Matrix R) -> Matrix R
unnormalize'  = uncurry unnormalize


unnormalize :: [(Double, Double)] -> Matrix R -> Matrix R
unnormalize meanStdDevs m = fromColumns cols1
  where
    cols0 = toColumns m :: [SV.Vector R]
    (means, stdDevs) = unzip meanStdDevs
    msdcs = zip3 means stdDevs cols0 :: [(Double, Double, SV.Vector R)]
    cols1 = map (\(mn, sd, col) -> SV.map (\el -> rescale (-1) (1) (mn - sd) (mn + sd) el) col) msdcs



--------------------------------------------------------------------------------



mtxsAreNearlyEqual :: Matrix R -> Matrix R -> Bool
mtxsAreNearlyEqual m1 m2 = ok
  where
    rows1 = toRows m1
    rows2  = toRows m2
    ok = and $ zipWith vectorsAreNearlyEqual rows1 rows2

vectorsAreNearlyEqual :: SV.Vector R -> SV.Vector R -> Bool
vectorsAreNearlyEqual v1 v2 = sameSize && ok
  where

    sameSize = size v1 == size v2
    
{-    n1 = norm_2 v1
    n2 = norm_2 v2
    n3 = sqrt $ n1*n1 + n2*n2
    tol = 1.0e-6 * (n1 + n2) -}

    els1 = toList v1
    els2 = toList v2

    ok = and $ zipWith nearlyEqual els1 els2

--------------------------------------------------------------------------------

io1 :: IO ()
io1 = do 
  g <- newStdGen

  let
    nPow = 1
    nData = 4
    td = generateTrainingData g nData nPow
--  putStrLn $ "td: " ++ show td
  
  let
    ntd = snd td :: NormalizedTrainingData
    w = solveNormalEquation (_X ntd) (_y ntd)
    
  putStrLn $ "w: " ++ show w

--  airports <- readFromFile "/Users/tad/tmp/leg_data/airports.txt"
  airports <- readFromFile "/home/tad/tmp/leg_data/airports.txt"
    :: IO [Airport]

  let nAirports = length airports

  putStrLn $ "nAirports: " ++ show nAirports

  let apMap = mkIcaoAirportMap airports

  let
    lax = apMap Map.! ICAO "KLAX"
    sfo = apMap Map.! ICAO "KSFO"
    teb = apMap Map.! ICAO "KTEB"

  putStrLn $ "lax: " ++ show lax
  putStrLn $ "sfo: " ++ show sfo
  putStrLn $ "teb: " ++ show teb

  let
    laxLatLon = apLatLon lax
-- **   sfoLatLon = apLatLon sfo
-- **   tebLatLon = apLatLon teb
    meanStdDevY = ntdMeanStdDevY ntd

    tebLatLon = mkLatLonDeg 1 0
    sfoLatLon = mkLatLonDeg 0 0
    
    (dist, distPred) =
      computeExactAndPredictedDistance tebLatLon sfoLatLon w meanStdDevY nPow


  putStrLn $ "dist: " ++ show dist
  putStrLn $ "distPred: " ++ show distPred
--  putStrLn $ " err: " ++ show err



--------------------------------------------------------------------------------

data Point = Point
  { px :: !Double
  , py :: !Double
  } deriving Show

io3 :: IO ()
io3 = do
  let
    nPow = 5
    nData = 10000
    nTests = 10000

  g <- newStdGen
  
  let
    (g1, g2) = split g
    td = generateCartsianDistanceTrainingData g1 nData nPow

--  putStrLn $ "td: " ++ show td

  let
    (_X, y) = snd td
    w = solveNormalEquation _X y

  putStrLn $ "w: " ++ show w

  let
    ptPairs = take nTests $ mkPairs $ randomPoints g2 :: [(Point, Point)]
--    (p1:p2:_) = pts
    meanStdDevY = undefined
    

    distAndPreds = 
      map (\(p1, p2) -> computeExactAndPredictedCartDist p1 p2 w meanStdDevY
                                                        nPow
          ) ptPairs :: [(Double, Double, Double)]

    -- errs = map (\ x y  -> abs (x - y)) distAndPreds :: [Double]

  putStrLn $ "length distAndPreds: " ++ show (length distAndPreds)

  
  forM_ (take 10 $ distAndPreds) showResults
--  forM_ distAndPreds showResults

  let
    --errs = map (\ (_, _, err) -> err) distAndPreds :: [Double]
    errs = SV.fromList $ map (\ (_, _, err) -> err) distAndPreds :: SV.Vector R
    mn = mean errs :: Double
    

  
  --putStrLn $ "errs: " ++ show errs
  putStrLn $ "mean: " ++ show mn
  

  putStrLn "done"


showResults :: (Double, Double, Double) -> IO ()
showResults (dist, distPred, err) = do
  putStrLn $ "dist:     " ++ show dist
  putStrLn $ "distPred: " ++ show distPred
  putStrLn $ "err:      " ++ show err
  putStrLn ""
    

distance :: Point -> Point -> Double
distance (Point x1 y1) (Point x2 y2)
  =  sqrt $ deltaX * deltaX + deltaY * deltaY
  where
    deltaX = x2 - x1
    deltaY = y2 - y1


generateCartsianDistanceTrainingData :: StdGen -> Int -> Int
        -> ([(Point, Point)], (Matrix R, SV.Vector R))
--        -> ([(Point, Point)], NormalizedTrainingData)

generateCartsianDistanceTrainingData g nData nPow =
  (pointPairs, normalizedTrainingData)

  where
    pointPairs = mkPairs $ take (2*nData) $ randomPoints g :: [(Point, Point)]
    normalizedTrainingData = mkTrainingCartDistData pointPairs nPow
      :: (Matrix R, SV.Vector R) -- NormalizedTrainingData

randomPoints :: StdGen -> [Point]
randomPoints g = zipWith Point xs ys
  where
    (g1, g2) = split g
    xs = randomRs (-10, 10) g1
    ys = randomRs (-10, 10) g2


mkTrainingCartDistData :: [(Point, Point)] -> Int
                       -> (Matrix R, SV.Vector R)
                   --    -> NormalizedCartDistTrainingData

mkTrainingCartDistData pointPairs nPow = (_X0, dists)
  where
    rowsWithPowers =
          map (\(p1, p2) -> mkTrainingCartDistRow nPow p1 p2)
              pointPairs

    _X0 = fromLists rowsWithPowers

--    (meansStdDevsX, _X1) = normalizeByColumns _X0
--      :: ([(Double, Double)], Matrix R)

    dists :: SV.Vector R
      = SV.fromList $ map (\(p1, p2) -> distance p1 p2) pointPairs

 --   (mn, sd, y) :: (Double, Double, SV.Vector R)
--      = normalizeVector dists

mkTrainingCartDistRow :: Int -> Point -> Point -> [Double]
mkTrainingCartDistRow nPow (Point x1 y1) (Point x2 y2) =
  mkNewVars (V.fromList [x1, y1, x2, y2]) nPow


computeExactAndPredictedCartDist :: Point -> Point
                                 -> SV.Vector R -> (Double, Double) -> Int
                                 -> (Double, Double, Double)

computeExactAndPredictedCartDist p1 p2 w meanStdDevY nPow =
  (dist, distP, err)
  where
    distP = predictCartDist p1 p2 w meanStdDevY nPow
    dist = distance p1 p2
    err = (abs $ distP - dist) / dist


predictCartDist :: Point -> Point
                -> SV.Vector R -> (Double, Double) -> Int
                -> Double

predictCartDist p1 p2 w meanStdDevY nPow = dist
  where
    mn = fst meanStdDevY
    sd = snd meanStdDevY

{-
    rawInput =
      RawInput (_latDeg p1) (_lonDeg p1)
               (_latDeg p2) (_lonDeg p2)
      
    Input1 x1 x2 x3 x4  = mkInput1 rawInput :: Input1
    inputV = V.fromList [x1, x2, x3, x4]
    vars = mkNewVars inputV nPow :: [Double]
-}
    vars = mkTrainingCartDistRow nPow p1 p2
    varsV = SV.fromList vars :: SV.Vector R
    distNorm = w <.> varsV
    dist = distNorm -- ** rescale (-1) 1 (mn - sd) (mn + sd) distNorm

--------------------------------------------------------------------------------
{-
dateStr0 = "2015-04-18 18:18:00"

t0 = parseTimeM False defaultTimeLocale "%F %T" dateStr0 :: Maybe UTCTime

t1 = readUTCTime dateStr0

xs1 = [1, 2, 3, 4]
xs2 = [5, 6, 7]


ys0 = ["1"]
-}

--------------------------------------------------------------------------------

{-
m0 :: Matrix R
m0 = (5 >< 3) [ 1,  5, 2
              , 0, -2, 9
              , 1, 11, -4
              , 4, 1,  8
              , 0, 9,  1
              ]

--m1 = tr m0


m1 = normalizeByColumns m0


m2 :: Matrix R
m2 = (1 >< 1) [0]
-}


{-
p0 = LatLonDeg 0 0
p1 = LatLonDeg 0 10

td1 = mkTrainingData [p0, p1] 2

-- example from http://mlwiki.org/index.php/Normal_Equation

_X0 :: Matrix R
_X0 = fromLists [[1, 1], [1, 2], [1, 3]]

y0 :: SV.Vector R
y0 = SV.fromList [1, 2, 2]

w0 = solveNormalEquation _X0 y0

mat = tr _X0 <> _X0
-}


p1 = Point 0 0
p2 = Point 1 1

td0 = mkTrainingCartDistData [(p1, p2)] 1

p3 = Point (-0.7919903958155528) (-0.382202268851777)
p4 = Point 0.5866289403041609 (-0.5393012876104206)
