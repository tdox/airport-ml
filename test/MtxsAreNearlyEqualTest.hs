module MtxsAreNearlyEqualTest where

import Lib

-- hmatrix
import Numeric.LinearAlgebra (Matrix, R, Vector, (¿), (><), flatten
                             , fromColumns, fromList, fromLists
                             , norm_2, range, row
                             , size, toList, toColumns, toRows, tr)


--------------------------------------------------------------------------------
-- unit tests


testMtxsAreNearlyEqual :: Bool
testMtxsAreNearlyEqual = ok
  where
    
    m0 :: Matrix R
    m0 = (5 >< 3) [ 1,  5, 2
                  , 0, -2, 9
                  , 1, 11, -4
                  , 4, 1,  8
                  , 0, 9,  1
                  ]

    m1 :: Matrix R
    m1 = (5 >< 3) [ 1,  5, 2
                  , 0, -2, 9
                  , 1, 11, -4
                  , 4, 1,  8
                  , 0, 9,  1.00001
                  ]
 
    ok1 = mtxsAreNearlyEqual m0 m0
    ok2 = mtxsAreNearlyEqual m1 m1
    ok3 = not $ mtxsAreNearlyEqual m0 m1

    ok = and [ok1, ok2, ok3]
