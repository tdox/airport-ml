{-# OPTIONS_GHC -Wall #-}
module Main where

import Lib
import MkNewVarsTest (testMkNewVars)
import MLTest
import NormalizeByColumnsTest
import MtxsAreNearlyEqualTest


-- base
import Control.Monad (when)
import System.Exit (exitFailure)
import Data.List (sort)

-- vector
import qualified Data.Vector as V


main :: IO ()
main = do

  when (not testMkNewVars) $ do
    putStrLn " *** testMkNewVars failed ***"
    exitFailure

  putStrLn "+++ OK, testMkNewVars passed"
    
  when (not testMtxsAreNearlyEqual) $ do
    putStrLn " *** testMtxsAreNearlyEqual failed ***"
    exitFailure

  putStrLn "+++ OK, passed testMtxsAreNearlyEqual"

  when (not unitTestNormalizeByColumns ) $ do
    putStrLn " ***  unitTestNormalizeByColumns failed ***"
    exitFailure

  putStrLn "+++ OK, unitTestNormalizeByColumns"


  quickChecks
