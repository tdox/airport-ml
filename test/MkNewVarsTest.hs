{-# OPTIONS_GHC -Wall #-}

module MkNewVarsTest (testMkNewVars) where

import Lib


-- base
import Data.List (sort)

-- vector
import qualified Data.Vector as V




testMkNewVars :: Bool
testMkNewVars = ok
  where
    ok = and [ok1, ok2, ok3, ok4, ok5, ok6, ok7, ok8]

ok1 :: Bool
ok1 = xs1 == xs1Std
  where
    xs0 = V.fromList [7]
    nPow = 0
    xs1 = mkNewVars xs0 nPow
    xs1Std = [1.0]

ok2 :: Bool
ok2 = xs1 == xs1Std
  where
    xs0 = V.fromList [7]
    nPow = 2
    xs1 = mkNewVars xs0 nPow
    xs1Std = [1.0, 7.0, 49.0]

ok3 :: Bool
ok3 = b
  where
    xs0 = V.fromList [2, 3]
    nPow = 0
    xs1 = mkNewVars xs0 nPow
    xs1Std = [1.0]
    b = xs1 == xs1Std

ok4 :: Bool
ok4 = b
  where
    xs0 = V.fromList [2, 3]
    nPow = 1
    xs1 = mkNewVars xs0 nPow
    xs1Std = [1.0, 3.0, 2.0]
    b = xs1 == xs1Std

ok5 :: Bool
ok5 = b
  where
    xs0 = V.fromList [2, 3]
    nPow = 2
    xs1 = mkNewVars xs0 nPow
    xs1Std = [1.0,3.0,9.0,2.0,6.0,4.0]
    b = xs1 == xs1Std

ok6 :: Bool
ok6 = b
  where
    xs0 = V.fromList [2, 3]
    nPow = 3
    xs1 = mkNewVars xs0 nPow
    xs1Std = [1.0, 2, 4, 8, 18, 12, 27, 3, 9, 6]
    b = sort xs1 == sort xs1Std

ok7 :: Bool
ok7 = b
  where
    xs0 = V.fromList [2, 3, 5]
    nPow = 1
    xs1 = mkNewVars xs0 nPow
    xs1Std = [1.0, 2, 3, 5]
    b = sort xs1 == sort xs1Std

ok8 :: Bool
ok8 = b
  where
    xs0 = V.fromList [2, 3, 5]
    nPow = 2
    xs1 = mkNewVars xs0 nPow
    xs1Std = [1.0, 2, 3, 5, 4, 6, 10, 9, 15, 25]
    b = sort xs1 == sort xs1Std
