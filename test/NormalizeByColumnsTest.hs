{-# LANGUAGE FlexibleInstances #-}
module NormalizeByColumnsTest where

import Lib

-- hmatrix
import Numeric.LinearAlgebra (Matrix, R, Vector, (¿), (><), flatten
                             , fromColumns, fromList, fromLists
                             , norm_2, range, row
                             , size, toList, toColumns, toRows, tr)
       
-- import qualified Numeric.LinearAlgebra.Data as HM

-- QuickCheck
import Test.QuickCheck ( Arbitrary, Gen, Positive, Result, arbitrary
                       , quickCheck, reason, sample
                       , suchThat, vector, vectorOf)

import Test.QuickCheck.Property (failed, succeeded)
import qualified Test.QuickCheck.Property as QCP

mkArbitraryMatrix :: Gen (Matrix R)
mkArbitraryMatrix = do
  nCols <- suchThat arbitrary (> 1) :: Gen Int
  nRows <- suchThat arbitrary (> 1) :: Gen Int
  els <- vectorOf (nRows * nCols) arbitrary :: Gen [Double]
  pure $ (nRows >< nCols) els :: Gen (Matrix R)
  

instance Arbitrary (Matrix R) where
   arbitrary = mkArbitraryMatrix

io1 :: IO ()
io1 = sample (arbitrary :: Gen (Matrix R))

quickChecks :: IO ()
quickChecks = do
  putStrLn "prop_normOfNormUnchanged"
  quickCheck prop_normOfNormUnchanged

  putStrLn "prop_meansOfNormOfNormAreZero"
  quickCheck prop_meansOfNormOfNormAreZero

  putStrLn "prop_stdDevsOfNormOfNormAreOne"
  quickCheck prop_stdDevsOfNormOfNormAreOne

  putStrLn "prop_unnormalizedNormalized"
  quickCheck prop_unnormalizedNormalized

  
prop_normOfNormUnchanged :: Gen QCP.Result
prop_normOfNormUnchanged = do
  m0 <- arbitrary :: Gen (Matrix R)

  let
    (meanStdDevs0, m1)  = normalizeByColumns m0
    (meanStdDevs1, m1') = normalizeByColumns m1
    ok = mtxsAreNearlyEqual m1 m1'

  return $ if ok
    then succeeded
    else failed -- { reason = "m1' /= m1"}

prop_meansOfNormOfNormAreZero :: Gen QCP.Result
prop_meansOfNormOfNormAreZero = do
  m0 <- arbitrary :: Gen (Matrix R)

  let
    (meanStdDevs0, m1)  = normalizeByColumns m0
    (meanStdDevs1, m1') = normalizeByColumns m1
    ok = and $ map (nearlyEqual 0.0) $ map fst meanStdDevs1

  return $ if ok
    then succeeded
    else failed -- { reason = "m1' /= m1"}


prop_stdDevsOfNormOfNormAreOne :: Gen QCP.Result
prop_stdDevsOfNormOfNormAreOne = do
  m0 <- arbitrary :: Gen (Matrix R)

  let
    (meanStdDevs0, m1)  = normalizeByColumns m0
    (meanStdDevs1, m1') = normalizeByColumns m1
    
    ok = and $ map (\x -> nearlyEqual 1 x || nearlyEqual 0 x)
                $ map snd meanStdDevs1

  return $ if ok
    then succeeded
    else failed -- { reason = "m1' /= m1"}


prop_unnormalizedNormalized :: Gen QCP.Result
prop_unnormalizedNormalized = do
  m0 <- arbitrary :: Gen (Matrix R)

  let
    (meanStdDevs0, m1)  = normalizeByColumns m0
    m0' = unnormalize meanStdDevs0 m1
    ok = mtxsAreNearlyEqual m0 m0'

  return $ if ok
    then succeeded
    else failed -- { reason = "m1' /= m1"}

  
--------------------------------------------------------------------------------
-- unit tests

unitTestNormalizeByColumns :: Bool
unitTestNormalizeByColumns = ok
  where
    ok = ok1 && ok2

    m1 :: Matrix R
    m1 = (1 >< 1) [0]

    ans1 = normalizeByColumns m1
    ans1' = ([(0.0,0.0)], row [ 0.0 ])

    ok1 = ans1 == ans1'

    m2 :: Matrix R
    m2 = fromLists [[-5, 0, 5]
                   ,[ 0, 5, 10]
                   , [5, 10, 15]]

    ans2 = normalizeByColumns m2
    ans2' = ([(0.0,5.0), (5.0,5.0), (10.0,5.0)]
            , fromLists [ [-1.0 :: Double, -1.0, -1.0]
                        , [ 0.0,  0.0,  0.0]
                        , [1.0,  1.0,  1.0 ]])

    ok2 = ans2 == ans2'



m0 :: Matrix R
m0 = (5 >< 3) [ 1,  5, 2
              , 0, -2, 9
              , 1, 11, -4
              , 4, 1,  8
              , 0, 9,  1
              ]


(meansStdDevs0, m1) = normalizeByColumns m0

m0' =  unnormalize meansStdDevs0 m1
